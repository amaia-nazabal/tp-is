package fr.univlyon1.tiw1.providedservices.metier;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@XmlRootElement(name = "commande")
public class Commande {
    public static class Item {
        public Item() {
        }

        public Item(String marchandiseId, int quantite) {
            this.marchandiseId = marchandiseId;
            this.quantite = quantite;
        }

        @XmlElement(name = "id")
        public String marchandiseId;
        public int quantite;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Item item = (Item) o;
            return quantite == item.quantite &&
                    Objects.equals(marchandiseId, item.marchandiseId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(marchandiseId, quantite);
        }
    }

    private Collection<Item> items = new ArrayList<>();

    public Collection<Item> getItems() {
        return items;
    }

    @XmlElement(name = "item")
    public void setItems(Collection<Item> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Commande commande = (Commande) o;
        return Objects.equals(items, commande.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(items);
    }
}
