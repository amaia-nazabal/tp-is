@XmlSchema(
        elementFormDefault = XmlNsForm.QUALIFIED,
        namespace = Constants.NAMESPACE)
package fr.univlyon1.tiw1.providedservices.metier;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;