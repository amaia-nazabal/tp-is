package fr.univlyon1.tiw1.providedservices.metier;

public class RuptureStockException extends Exception {
    public RuptureStockException() {
    }

    public RuptureStockException(String message) {
        super(message);
    }

    public RuptureStockException(String message, Throwable cause) {
        super(message, cause);
    }

    public RuptureStockException(Throwable cause) {
        super(cause);
    }

    public RuptureStockException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
