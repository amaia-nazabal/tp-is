package fr.univlyon1.tiw1.providedservices.metier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

@WebService(targetNamespace = Constants.NAMESPACE)
public class MagasinService {

    private static final Logger LOG = LoggerFactory.getLogger(MagasinService.class);
    private static DatatypeFactory dtf;

    static {
        try {
            dtf = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            throw new Error(e);
        }
    }

    @Autowired
    private MarchandiseRepository marchandiseRepository;

    @Transactional
    public void approvisionne(@WebParam(name = "marchandise") String marchandiseId,
                              @WebParam(name = "quantite") int quantite) {
        if (marchandiseRepository.exists(marchandiseId)) {
            Marchandise m = marchandiseRepository.findOne(marchandiseId);
            m.setQuantite(m.getQuantite() + quantite);
            LOG.info("Marchandise {} mise à jour -> {}", marchandiseId, m.getQuantite());
        } else {
            Marchandise m = new Marchandise();
            m.setId(marchandiseId);
            m.setQuantite(quantite);
            marchandiseRepository.save(m);
            LOG.info("Marchandise {} créée -> {}", marchandiseId, m.getQuantite());
        }
    }

    @WebResult(name = "dateLivraison")
    @Transactional
    public XMLGregorianCalendar commander(@WebParam(name = "commande") Commande commande) throws RuptureStockException {
        LOG.info("Commande: {}", commande);
        for (Commande.Item item : commande.getItems()) {
            if (!(marchandiseRepository.exists(item.marchandiseId)
                    && marchandiseRepository.findOne(item.marchandiseId).getQuantite() >= item.quantite)) {
                throw new RuptureStockException(String.format("Marchandise en quantité insuffisante: %1$ (%2$)",
                        item.marchandiseId, item.quantite));
            }
        }
        for (Commande.Item item : commande.getItems()) {
            Marchandise m = marchandiseRepository.findOne(item.marchandiseId);
            m.setQuantite(m.getQuantite() - item.quantite);
        }
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());
        return dtf.newXMLGregorianCalendar(c);
    }
}
