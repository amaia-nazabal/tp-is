package fr.univlyon1.tiw1.providedservices.metier;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface MarchandiseRepository extends JpaRepository<Marchandise, String> {
}
