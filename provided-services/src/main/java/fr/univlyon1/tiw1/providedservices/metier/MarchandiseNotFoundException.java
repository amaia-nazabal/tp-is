package fr.univlyon1.tiw1.providedservices.metier;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MarchandiseNotFoundException extends RuntimeException {
    public MarchandiseNotFoundException() {
    }

    public MarchandiseNotFoundException(String message) {
        super(message);
    }

    public MarchandiseNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public MarchandiseNotFoundException(Throwable cause) {
        super(cause);
    }

    public MarchandiseNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
