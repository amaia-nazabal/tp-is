package fr.univlyon1.tiw1.providedservices.metier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
public class MarchandiseResource {

    private static final Logger LOG = LoggerFactory.getLogger(MarchandiseResource.class);

    @Autowired
    private MarchandiseRepository marchandiseRepository;

    @GetMapping("/marchandises/{id}")
    public Marchandise get(@PathVariable(name = "id") String id) {
        Marchandise m = marchandiseRepository.findOne(id);
        LOG.info("Retreiving marchandise (id={}): {}", id, m);
        if (m == null) {
            throw new MarchandiseNotFoundException("Marchandise " + id + " non trouvee");
        } else {
            return m;
        }
    }

    @PostMapping("/marchandises")
    @Transactional
    public Marchandise post(Marchandise marchandise) {
        return marchandiseRepository.save(marchandise);
    }

    @PutMapping("/marchandises/{id}")
    @Transactional
    public Marchandise put(@PathVariable(name = "id") String id, Marchandise marchandise) {
        if (!marchandise.getId().equals(id)){
            throw new ErrorInRequestException(String.format("Ids incoherents: %1$ et %2$", id, marchandise.getId()));
        } else {
            return marchandiseRepository.save(marchandise);
        }
    }


}
