package fr.univlyon1.tiw1.providedservices.metier;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class ErrorInRequestException extends RuntimeException {
    public ErrorInRequestException() {
    }

    public ErrorInRequestException(String message) {
        super(message);
    }

    public ErrorInRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public ErrorInRequestException(Throwable cause) {
        super(cause);
    }

    public ErrorInRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
