package fr.univlyon1.tiw1.providedservices;

import fr.univlyon1.tiw1.providedservices.metier.MagasinService;
import fr.univlyon1.tiw1.providedservices.metier.MarchandiseResource;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.instrument.classloading.LoadTimeWeaver;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.sql.DataSource;
import javax.xml.ws.Endpoint;

@SpringBootApplication
// Activation du support des transactions déclaratives
@EnableTransactionManagement
// Activation de la génération de description de ressources REST en Swagger
@EnableSwagger2
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    // Config from https://cwiki.apache.org/confluence/display/CXF20DOC/SpringBoot
    @Autowired
    private Bus bus;

    @Bean
    public DataSource embeddedDB() {
        return new EmbeddedDatabaseBuilder()
                .generateUniqueName(true)
                .setType(EmbeddedDatabaseType.H2)
                .setScriptEncoding("UTF-8")
                .ignoreFailedDrops(true)
                .addScript("/db/schema.sql")
                .addScript("/db/data.sql")
                .build();
    }

    @Bean
    public LoadTimeWeaver weaver() {
        return new InstrumentationLoadTimeWeaver();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
        emfb.setDataSource(embeddedDB());
        emfb.setPersistenceUnitName("marchandises-pu");
        emfb.setLoadTimeWeaver(weaver());
        return emfb;
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        final JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setDataSource(embeddedDB());
        return jpaTransactionManager;
    }

    @Bean
    public MagasinService magasinService() {
        return new MagasinService();
    }

    @Bean
    public Endpoint endpointMagasin() {
        EndpointImpl endpoint = new EndpointImpl(bus, magasinService());
        endpoint.publish("/magasin");
        return endpoint;
    }
}
