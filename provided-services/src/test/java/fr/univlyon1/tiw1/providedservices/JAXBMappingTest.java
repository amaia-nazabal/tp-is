package fr.univlyon1.tiw1.providedservices;

import fr.univlyon1.tiw1.providedservices.metier.Commande;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

public class JAXBMappingTest {
    @Test
    public void testCommandeMapping() throws JAXBException {
        Commande c = new Commande();
        Collection<Commande.Item> items = new ArrayList<>();
        items.add(new Commande.Item("m1", 12));
        items.add(new Commande.Item("m2", 23));
        c.setItems(items);
        JAXBContext jc = JAXBContext.newInstance(Commande.class);
        StringWriter sw = new StringWriter();
        jc.createMarshaller().marshal(c,sw);
        StringReader sr = new StringReader((sw.toString()));
        Commande c2 = (Commande) jc.createUnmarshaller().unmarshal(sr);
        assertEquals(c,c2);
    }
}
