package fr.univlyon1.tiw1.tp5.modele;

import com.fasterxml.jackson.annotation.JsonFormat;
import fr.univlyon1.tiw1.metier.spec.Approvisionnement;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/14/17.
 */

@Entity
@Table(name = "approvisionnement")
@NamedQueries({
        @NamedQuery(name = "allApprovisionnements", query = "SELECT a FROM ApprovisionnementModele a"),
        @NamedQuery(name = "approvisionnementsFromEntrepot", query = "SELECT a FROM ApprovisionnementModele a JOIN a.entrepot e WHERE e.nom = :nom"),
        @NamedQuery(name = "approvisionnementsFromMarchandise", query = "SELECT l FROM ApprovisionnementModele l JOIN l.marchandise m WHERE m.reference = :ref"),
        @NamedQuery(name = "approvisionnementsFromFournisseur", query = "SELECT l FROM ApprovisionnementModele l  WHERE l.fournisseur = :fournisseur")})

public class ApprovisionnementModele implements Approvisionnement {
    @Id
    @GeneratedValue(generator = "hibernate-sequence")
    @SequenceGenerator(name = "hibernate-sequence", allocationSize = 1)
    private Integer id;

    private String fournisseur;
    @Temporal(TemporalType.DATE)
    @Column(name = "prevu")
    private Date datePrevue;
    @Temporal(TemporalType.DATE)
    @Column(name = "effectue")
    private Date dateEffectuee = null;
    @ManyToOne
    @JoinColumn(name = "ref_m")
    protected MarchandiseModele marchandise;
    @ManyToOne
    @JoinColumn(name = "nom_e")
    protected EntrepotModele entrepot;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation")
    private Date dateCreation;
    private int quantite;

    public ApprovisionnementModele() {
        // Empty constructor for JPA
    }

    /**
     * Construit un nouvel approvisionnement
     *
     * @param marchandise la marchandise
     * @param entrepot    l'entrepot
     * @param fournisseur le fournisseur
     * @param quantite    la quantite
     * @param creation    la date de creation. Si cette date est nulle, la création est initialisée à la date courante
     * @param datePrevue  la date de livraison prévue.
     */
    public ApprovisionnementModele(MarchandiseModele marchandise, EntrepotModele entrepot, String fournisseur, int quantite, Date creation, Date datePrevue) {
        this.fournisseur = fournisseur;
        this.datePrevue = datePrevue;
        this.dateCreation = creation;
        this.marchandise = marchandise;
        this.entrepot = entrepot;
        this.quantite = quantite;
        if (this.dateCreation == null) {
            this.dateCreation = new Date();
        }
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getFournisseur() {
        return fournisseur;
    }

    @Override
    public int getQuantite() {
        return quantite;
    }

    @Override
    public int getRefMarchandise() {
        return marchandise.getReference();
    }

    @Override
    public String getNomEntrepot() {
        return entrepot.getNom();
    }

    @Override
    public Date getDateCreation() {
        return dateCreation;
    }

    @Override
    public Date getDateEffectuee() {
        return dateEffectuee;
    }

    @Override
    public Date getDatePrevue() {
        return datePrevue;
    }

    public void setFournisseur(String fournisseur) {
        this.fournisseur = fournisseur;
    }


    public void setDateCreation(Date creation) {
        this.dateCreation = creation;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }


    public void setMarchandise(MarchandiseModele marchandise) {
        this.marchandise = marchandise;
    }

    public void setEntrepot(EntrepotModele entrepot) {
        this.entrepot = entrepot;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonFormat(pattern="MM-dd-yyyy HH:mm:ss.SS")
    public void setDateEffectuee(Date dateEffectuee) {
        this.dateEffectuee = dateEffectuee;
    }

    @JsonFormat(pattern="MM-dd-yyyy HH:mm:ss.SS")
    public void setDatePrevue(Date datePrevue) {
        this.datePrevue = datePrevue;
    }
}

