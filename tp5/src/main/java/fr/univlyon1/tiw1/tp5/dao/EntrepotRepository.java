package fr.univlyon1.tiw1.tp5.dao;


import fr.univlyon1.tiw1.metier.spec.Entrepot;
import fr.univlyon1.tiw1.metier.spec.Marchandise;
import fr.univlyon1.tiw1.metier.spec.OperationFailedException;
import fr.univlyon1.tiw1.metier.spec.dao.EntrepotDAO;
import fr.univlyon1.tiw1.tp5.modele.EntrepotModele;
import fr.univlyon1.tiw1.tp5.modele.MarchandiseModele;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 11/15/17.
 *
 * If I notice that I have to use the entity manager by autowire I have to make sure than the annotation transactional
 * is present
 */
@Repository
public class EntrepotRepository implements EntrepotDAO {

    private EntityManager entityManager;
    private MarchandiseRepository marchandiseDAO;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Autowired
    public void setMarchandiseDAO(MarchandiseRepository marchandiseDAO) {
        this.marchandiseDAO = marchandiseDAO;
    }

    @Override
    public EntrepotModele findByNom(String nom) {
        return entityManager.find(EntrepotModele.class, nom);
    }

    @Override
    public Collection<Entrepot> getAllEntrepots() {
        return Collections.unmodifiableCollection(entityManager
                .createNamedQuery("allEntrepots", EntrepotModele.class)
                .getResultList());
    }

    @Override
    public Entrepot createOrUpdate(Entrepot entrepot) throws OperationFailedException {
        EntrepotModele managed = findByNom(entrepot.getNom());
        if (managed == null) {
            managed = EntrepotModele.asJPA(entrepot);
            entityManager.persist(managed);
        }
        if (managed != entrepot) {
            // fixme check if its okay
            managed.setCapacite(entrepot.getCapacite());

            Map<MarchandiseModele, Integer> newStock = new HashMap<>();
            for (Marchandise m : entrepot.getMarchandisesStockees()) {
                int qte = entrepot.getStock(m);
                if (qte != 0) {
                    Marchandise jm = marchandiseDAO.createOrUpdate(m);
                    newStock.put((MarchandiseModele)jm, qte);
                }
            }
            managed.updateStock(newStock);
        }
        return managed;
    }

    @Override
    public void remove(Entrepot entrepot) throws OperationFailedException {
        EntrepotModele e = findByNom(entrepot.getNom());
        if (e != null) {
            entityManager.remove(e);
        }
    }
}
