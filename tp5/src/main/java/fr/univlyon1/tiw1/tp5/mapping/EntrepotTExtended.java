package fr.univlyon1.tiw1.tp5.mapping;

import fr.univ_lyon1.master_info.tiw.tiw1._2017.entrepots.EntrepotT;
import fr.univ_lyon1.master_info.tiw.tiw1._2017.entrepots.StockT;
import fr.univlyon1.tiw1.metier.spec.Entrepot;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/14/17.
 */
public class EntrepotTExtended extends EntrepotT{
    public EntrepotTExtended(String nom, double capacite) {
        this.nom = nom;
        this.capacite = capacite;
    }

    public EntrepotTExtended(String nom, double capacite, List<StockT> marchandise) {
        this.nom = nom;
        this.capacite = capacite;
        this.marchandise = marchandise;
    }



    public void setMarchandise(List<StockT> marchandise) {
        this.marchandise = marchandise;
    }

    public static EntrepotTExtended toEntrepotT(Entrepot entrepot) {

        List<StockT> list = new ArrayList<>();
            entrepot.getMarchandisesStockees().forEach(m -> {
                StockT stockT = new StockT();
                stockT.setRef(m.getReference());
                stockT.setValue(entrepot.getStock(m));
                list.add(stockT);
            });

        return new EntrepotTExtended(entrepot.getNom(), entrepot.getCapacite(), list);
    }
}
