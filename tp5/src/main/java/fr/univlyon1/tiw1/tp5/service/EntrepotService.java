package fr.univlyon1.tiw1.tp5.service;

import fr.univlyon1.tiw1.metier.base.EntrepotOperationsImpl;
import fr.univlyon1.tiw1.metier.spec.*;
import fr.univlyon1.tiw1.metier.spec.dao.ApprovisionnementDAO;
import fr.univlyon1.tiw1.metier.spec.dao.EntrepotDAO;
import fr.univlyon1.tiw1.metier.spec.dao.LivraisonDAO;
import fr.univlyon1.tiw1.metier.spec.dao.MarchandiseDAO;
import fr.univlyon1.tiw1.tp5.service.exception.DataNotFoundException;
import fr.univlyon1.tiw1.tp5.service.exception.OperationFailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 11/15/17.
 *
 * Layer of service for the warehouse.
 */
@Service
public class EntrepotService extends EntrepotOperationsImpl {

    @Autowired
    public EntrepotService(EntrepotDAO edao, LivraisonDAO ldao,
                           ApprovisionnementDAO adao, MarchandiseDAO mdao) {
        super(edao, ldao, adao, mdao);
    }

    /**
     * Retrieve the warehouse by name
     * @param nom the name of the warehouse
     * @return the warehouse entity
     * @throws DataNotFoundException if any warehouse with this name exists.
     */
    @Transactional
    public Entrepot getByName(String nom) {
        Entrepot entrepot = super.getByNom(nom);
        if (entrepot == null)
            throw new DataNotFoundException("L'entrepot n'existe pas.");

        return entrepot;
    }

    /**
     * Retrieve all the warehouses
     * @return the list of the warehouses.
     */
    public Collection<Entrepot> getAll() {
        return this.edao.getAllEntrepots();
    }

    /**
     * Remove a specific ware house by name
     *
     * @param nom the name of the warehouse
     * @throws OperationFailException if it's not possible remove the entity.
     */
    public void remove(String nom) throws OperationFailException {
        Entrepot entrepot = super.getByNom(nom);
        if (entrepot == null)
            throw new DataNotFoundException("L'entrepot n'existe pas");

        try {
            super.edao.remove(entrepot);
        } catch (OperationFailedException e) {
            throw new OperationFailException(e);
        }
    }

    @Transactional
    @Override
    public Approvisionnement creeApprovisionnement(Entrepot entrepot, Marchandise marchandise, String fournisseur, int quantite, Date datePrevue) throws OperationFailException {
        try {
            return super.creeApprovisionnement(entrepot, marchandise, fournisseur, quantite, datePrevue);
        } catch (OperationFailedException e) {
            throw new OperationFailException(e);
        }
    }

    @Transactional
    @Override
    public Livraison creeLivraison(Entrepot entrepot, Marchandise marchandise, String magasin, int quantite, Date datePrevue) throws OperationFailedException {
        return super.creeLivraison(entrepot, marchandise, magasin, quantite, datePrevue);
    }

    @Transactional
    @Override
    public void createOrUpdate(Entrepot entrepot) throws OperationFailedException {
        super.createOrUpdate(entrepot);
    }

    @Transactional
    @Override
    public void livrer(Livraison livraison, Date dateLivraison) throws OperationFailedException {
        super.livrer(livraison, dateLivraison);
    }
}
