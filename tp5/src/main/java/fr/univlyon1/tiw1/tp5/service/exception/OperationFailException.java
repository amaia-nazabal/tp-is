package fr.univlyon1.tiw1.tp5.service.exception;

import fr.univlyon1.tiw1.metier.spec.OperationFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/6/17.
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "L'operation ne peut pas être effectue.")
public class OperationFailException extends OperationFailedException {
    public OperationFailException(OperationFailedException e) {
        super(e.getMessage(), e.getCause());
    }

}
