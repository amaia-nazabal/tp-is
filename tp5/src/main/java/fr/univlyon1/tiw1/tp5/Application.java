package fr.univlyon1.tiw1.tp5;

import fr.univlyon1.tiw1.tp5.controller.ProviderController;
import fr.univlyon1.tiw1.tp5.service.EntrepotPortTypeImpl;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.instrument.classloading.LoadTimeWeaver;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import javax.xml.ws.Endpoint;
import java.util.TimeZone;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/13/17.
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    private Bus bus;

    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Bean
    public DataSource embeddedDB() {
        return new EmbeddedDatabaseBuilder()
                .generateUniqueName(true)
                .setType(EmbeddedDatabaseType.H2)
                .setScriptEncoding("UTF-8")
                .ignoreFailedDrops(true)
                .addScript("/db/schema.sql")
                .addScript("/db/data.sql")
                .build();
    }

    @Bean
    public LoadTimeWeaver weaver() {
        return new InstrumentationLoadTimeWeaver();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
        emfb.setPackagesToScan("fr.univlyon1.tiw1.tp5");
        emfb.setDataSource(embeddedDB());
        emfb.setPersistenceUnitName("connection-pu");
        emfb.setLoadTimeWeaver(weaver());

        return emfb;
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        final JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setDataSource(embeddedDB());
        return jpaTransactionManager;
    }

    @Bean
    public EntrepotPortTypeImpl entrepotPortTypeImpl() {
        return new EntrepotPortTypeImpl();
    }

    @Bean
    public ProviderController providerController() {
        return new ProviderController();
    }

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, entrepotPortTypeImpl());
        endpoint.publish("/entrepot");

        return endpoint;
    }

    @Bean
    public Endpoint endpoint2() {
        EndpointImpl endpoint = new EndpointImpl(bus, providerController());
        endpoint.publish("/provider");

        return endpoint;
    }


}
