package fr.univlyon1.tiw1.tp5.mapping;

import fr.univ_lyon1.master_info.tiw.tiw1._2017.entrepots.ApprovisionnementT;
import fr.univlyon1.tiw1.metier.spec.Approvisionnement;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.GregorianCalendar;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/14/17.
 */
public class ApprovisionnementTExtended extends ApprovisionnementT {

    public ApprovisionnementTExtended() {

    }

    public ApprovisionnementTExtended(String id, int ref, String entrepot, int quantite, XMLGregorianCalendar datePrevue) {
        this.id = id;
        this.ref = ref;
        this.entrepot = entrepot;
        this.quantite = quantite;
        this.datePrevue = datePrevue;
    }

    private ApprovisionnementTExtended(String id, int ref, String entrepot, int quantite, XMLGregorianCalendar datePrevue,
                                       XMLGregorianCalendar dateCreation) {
        this.id = id;
        this.ref = ref;
        this.entrepot = entrepot;
        this.quantite = quantite;
        this.datePrevue = datePrevue;
        this.dateCreation = dateCreation;
    }

    private ApprovisionnementTExtended(String id, int ref, String entrepot, int quantite, XMLGregorianCalendar datePrevue,
                                      XMLGregorianCalendar dateCreation, XMLGregorianCalendar dateEffective) {
        this.id = id;
        this.ref = ref;
        this.entrepot = entrepot;
        this.quantite = quantite;
        this.datePrevue = datePrevue;
        this.dateCreation = dateCreation;
        this.dateEffective = dateEffective;
    }

    public static ApprovisionnementT toApprovisionnementT (Approvisionnement approvisionnement) throws DatatypeConfigurationException {

        GregorianCalendar datePrevue = new GregorianCalendar();
        datePrevue.setTime(approvisionnement.getDatePrevue());

        GregorianCalendar dateCreation = new GregorianCalendar();
        datePrevue.setTime(approvisionnement.getDateCreation());

        if (approvisionnement.getDateEffectuee() != null) {
            GregorianCalendar dateEffectuee = new GregorianCalendar();
            datePrevue.setTime(approvisionnement.getDateEffectuee());

            return new ApprovisionnementTExtended(String.valueOf(approvisionnement.getId()),
                    approvisionnement.getRefMarchandise(), approvisionnement.getNomEntrepot(), approvisionnement.getQuantite(),
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(datePrevue),
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(dateCreation),
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(dateEffectuee));
        } else {
            return new ApprovisionnementTExtended(String.valueOf(approvisionnement.getId()),
                    approvisionnement.getRefMarchandise(), approvisionnement.getNomEntrepot(), approvisionnement.getQuantite(),
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(datePrevue),
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(dateCreation));
        }
    }
}
