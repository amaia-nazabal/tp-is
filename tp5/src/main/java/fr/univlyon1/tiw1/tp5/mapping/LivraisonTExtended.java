package fr.univlyon1.tiw1.tp5.mapping;

import fr.univ_lyon1.master_info.tiw.tiw1._2017.entrepots.LivraisonT;
import fr.univlyon1.tiw1.metier.spec.Livraison;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.GregorianCalendar;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/14/17.
 */
public class LivraisonTExtended extends LivraisonT {
    public LivraisonTExtended(String id, String entrepot, int ref, String magasin, XMLGregorianCalendar datePrevue,
                              int quantite, XMLGregorianCalendar dateCreation) {
        this.magasin = magasin;
        this.entrepot = entrepot;
        this.ref = ref;
        this.datePrevue = datePrevue;
        this.id = id;
        this.quantite = quantite;
        this.dateCreation = dateCreation;
    }

    private LivraisonTExtended(String id, int ref, String entrepot, int quantite, XMLGregorianCalendar datePrevue,
                               XMLGregorianCalendar dateCreation) {
        this.id = id;
        this.ref = ref;
        this.entrepot = entrepot;
        this.quantite = quantite;
        this.datePrevue = datePrevue;
        this.dateCreation = dateCreation;
    }

    private LivraisonTExtended(String id, int ref, String entrepot, int quantite, XMLGregorianCalendar datePrevue,
                               XMLGregorianCalendar dateCreation, XMLGregorianCalendar dateEffective) {
        this.id = id;
        this.ref = ref;
        this.entrepot = entrepot;
        this.quantite = quantite;
        this.datePrevue = datePrevue;
        this.dateCreation = dateCreation;
        this.dateEffective = dateEffective;
    }

    public static LivraisonTExtended toLivraisonT(Livraison livraison) throws DatatypeConfigurationException {

        GregorianCalendar datePrevue = new GregorianCalendar();
        datePrevue.setTime(livraison.getDatePrevue());

        GregorianCalendar dateCreation = new GregorianCalendar();
        datePrevue.setTime(livraison.getDateCreation());

        if (livraison.getDateEffectuee() != null ) {
            GregorianCalendar dateEffectuee = new GregorianCalendar();
            datePrevue.setTime(livraison.getDateEffectuee());

            return new LivraisonTExtended(String.valueOf(livraison.getId()),
                    livraison.getRefMarchandise(), livraison.getNomEntrepot(), livraison.getQuantite(),
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(datePrevue),
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(dateCreation),
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(dateEffectuee));
        } else
            return new LivraisonTExtended(String.valueOf(livraison.getId()),
                    livraison.getRefMarchandise(), livraison.getNomEntrepot(), livraison.getQuantite(),
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(datePrevue),
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(dateCreation));
    }

}
