package fr.univlyon1.tiw1.tp5.dao;

import fr.univlyon1.tiw1.metier.spec.Approvisionnement;
import fr.univlyon1.tiw1.metier.spec.OperationFailedException;
import fr.univlyon1.tiw1.metier.spec.dao.ApprovisionnementDAO;
import fr.univlyon1.tiw1.tp5.modele.ApprovisionnementModele;
import fr.univlyon1.tiw1.tp5.modele.EntrepotModele;
import fr.univlyon1.tiw1.tp5.modele.MarchandiseModele;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 11/15/17.
 */
@Repository
public class ApprovisionnementRepository implements ApprovisionnementDAO {

    private EntityManager entityManager;
    private EntrepotRepository edao;
    private MarchandiseRepository mdao;


    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Autowired
    public void setEdao(EntrepotRepository edao) {
        this.edao = edao;
    }

    @Autowired
    public void setMdao(MarchandiseRepository mdao) {
        this.mdao = mdao;
    }

    @Override
    public ApprovisionnementModele findById(int id) {
        return entityManager.find(ApprovisionnementModele.class, id);
    }

    @Override
    public Collection<Approvisionnement> fromEntrepot(String nom) {
        return Collections.unmodifiableCollection(
                entityManager.createNamedQuery("approvisionnementsFromEntrepot", ApprovisionnementModele.class)
                        .setParameter("nom", nom)
                        .getResultList()
                                                 );
    }

    @Override
    public Collection<Approvisionnement> fromMarchandise(int ref) {
        return Collections.unmodifiableCollection(
                entityManager.createNamedQuery("approvisionnementsFromMarchandise", ApprovisionnementModele.class)
                        .setParameter("ref", ref)
                        .getResultList()
                                                 );
    }

    @Override
    public Collection<Approvisionnement> fromFournisseur(String nom) {
        return Collections.unmodifiableCollection(
                entityManager.createNamedQuery("approvisionnementsFromFournisseur", ApprovisionnementModele.class)
                        .setParameter("fournisseur", nom)
                        .getResultList()
                                                 );
    }

    @Transactional
    @Override
    public Approvisionnement createOrUpdate(Approvisionnement approvisionnement) throws OperationFailedException {
        ApprovisionnementModele jl = findById(approvisionnement.getId());
        if (jl == null || !(approvisionnement instanceof ApprovisionnementModele)) {
            boolean exists = jl != null;
            jl = copyApprovisionnementModele(approvisionnement);
            if (exists) {
                return entityManager.merge(jl);
            } else {
                entityManager.persist(jl);
                return jl;
            }
        } else if (jl == approvisionnement) {
            return approvisionnement;
        } else {
            return entityManager.merge(approvisionnement);
        }
    }

    private ApprovisionnementModele copyApprovisionnementModele(Approvisionnement approvisionnement) throws OperationFailedException {
        ApprovisionnementModele jl;
        EntrepotModele e = edao.findByNom(approvisionnement.getNomEntrepot());
        if (e == null) {
            throw new OperationFailedException("pas d'entrepot correspondant trouvé");
        }
        MarchandiseModele m = mdao.findByRef(approvisionnement.getRefMarchandise());
        if (m == null) {
            throw new OperationFailedException("pas de marchandise trouvée");
        }
        jl = new ApprovisionnementModele(m, e, approvisionnement.getFournisseur(), approvisionnement.getQuantite(), approvisionnement.getDateCreation(), approvisionnement.getDatePrevue());
        jl.setDateEffectuee(approvisionnement.getDateEffectuee());
        return jl;
    }

    @Override
    public void remove(Approvisionnement approvisionnement) throws OperationFailedException {
        ApprovisionnementModele ja = findById(approvisionnement.getId());
        if (ja != null) {
            entityManager.remove(ja);
        }
    }

    @Override
    public Collection<Approvisionnement> getAllApprovisionnements() throws OperationFailedException {
        return Collections.unmodifiableCollection(
                entityManager.createNamedQuery("allApprovisionnements", ApprovisionnementModele.class)
                        .getResultList()
                                                 );
    }
}
