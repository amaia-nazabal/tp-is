package fr.univlyon1.tiw1.tp5.controller;

import fr.univlyon1.tiw1.metier.spec.OperationFailedException;
import fr.univlyon1.tiw1.tp5.modele.EntrepotModele;
import fr.univlyon1.tiw1.tp5.service.ApprovisionnementService;
import fr.univlyon1.tiw1.tp5.service.EntrepotService;
import fr.univlyon1.tiw1.tp5.service.LivraisonService;
import fr.univlyon1.tiw1.tp5.service.MarchandiseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceProvider;
import java.io.ByteArrayInputStream;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/15/17.
 */
@WebServiceProvider(portName = "examplePort",
serviceName = "exampleService",
targetNamespace = "http://master-info.univ-lyon1.fr/TIW/TIW1/2017/entrepots/services/entrepot",
wsdlLocation = "/static/entrepot.wsdl")
@ServiceMode(Service.Mode.MESSAGE)
public class ProviderController implements Provider<SOAPMessage> {
    private static final Logger LOG = LoggerFactory.getLogger(ProviderController.class);

    @Autowired
    private EntrepotService entrepotService;

    @Autowired
    private MarchandiseService marchandiseService;

    @Autowired
    private LivraisonService livraisonService;

    @Autowired
    private ApprovisionnementService approvisionnementService;

    @Override
    public SOAPMessage invoke(SOAPMessage message) {
        Source result = null;
        try {
//            JAXBContext context = JAXBContext.newInstance(EntrepotT.class);
//            Unmarshaller unmarshaller = context.createUnmarshaller();

//            DOMResult dom = new DOMResult();
//            Transformer transformer = TransformerFactory.newInstance().newTransformer();
//            transformer.transform(source, dom);

//            XMLStreamReader xsr = message.createXMLStreamReader(xml);
//            xsr.nextTag();
//            while(!xsr.getLocalName().equals("return")) {
//                xsr.nextTag();
//            }

            JAXBContext jc = JAXBContext.newInstance(Entrepot.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            Entrepot entrepot = (Entrepot)unmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument());


//            EntrepotT element = (EntrepotT) unmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument());
            System.out.println(entrepot.getNom());



//            String operation = dom.getNode().getFirstChild().getLocalName();
//            switch (operation) {
//                case Constant.CREATE_OR_UPDATE_WARE:
//                    String wareName = dom.getNode().getFirstChild().getFirstChild().getNextSibling().getFirstChild().getNextSibling().getTextContent();
//                    String wareCapacity = dom.getNode().getFirstChild().getFirstChild().getNextSibling().getFirstChild().getNextSibling().getNextSibling().getNextSibling().getTextContent();
//
//                    result = operationCreateWare(wareName, Double.parseDouble(wareCapacity));
//                    break;
//                case Constant.GET_WARE_BY_NAME:
//                    getWareByName();
//                    break;
//
//                default:
//                    throw new OperationNotSupportedException("N'est pas supporte.");
//            }
//
//

//            System.out.println(dom.getSystemId());


        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
        return null;
    }

    private void getWareByName() {

    }

    private Source operationCreateWare(String wareName, double wareCapacity) {

        try {
            EntrepotModele entrepot = new EntrepotModele(wareName, wareCapacity);
            entrepotService.createOrUpdate(entrepot);
        } catch (OperationFailedException e) {
            LOG.error(e.getMessage());
            //todo fail
        }

        String body =
                "<createUpdateAck xmlns=\"http://master-info.univ-lyon1.fr/TIW/TIW1/2017/entrepots/services/entrepot\" " +
                        "xmlns:ns2=\"http://master-info.univ-lyon1.fr/TIW/TIW1/2017/entrepots\">" +
                        "true</createUpdateAck>";


        return new StreamSource(new ByteArrayInputStream(body.getBytes()));
    }

    private Source sendSource(String input) {
        String body =
                "<createUpdateAck xmlns=\"http://master-info.univ-lyon1.fr/TIW/TIW1/2017/entrepots/services/entrepot\" " +
                        "xmlns:ns2=\"http://master-info.univ-lyon1.fr/TIW/TIW1/2017/entrepots\">" +
                        "true</createUpdateAck>";

        return new StreamSource(new ByteArrayInputStream(body.getBytes()));
    }
}
