package fr.univlyon1.tiw1.tp5.dao;

import fr.univlyon1.tiw1.metier.spec.Marchandise;
import fr.univlyon1.tiw1.metier.spec.OperationFailedException;
import fr.univlyon1.tiw1.metier.spec.dao.MarchandiseDAO;
import fr.univlyon1.tiw1.tp5.modele.MarchandiseModele;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 11/15/17.
 */
@Repository
public class MarchandiseRepository implements MarchandiseDAO {
    @PersistenceContext
    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public MarchandiseModele findByRef(int ref) {
        return entityManager.find(MarchandiseModele.class, ref);
    }

    @Override
    public Collection<Marchandise> getAllMarchandises() {
        return Collections.unmodifiableCollection(entityManager
                .createNamedQuery("allMarchandises", MarchandiseModele.class)
                .getResultList());
    }

    @Override
    public Marchandise createOrUpdate(Marchandise marchandise) throws OperationFailedException {
        MarchandiseModele managed = findByRef(marchandise.getReference());
        if (managed == null) {
            managed = MarchandiseModele.asJPA(marchandise);
            if (marchandise.getReference() == -1) {
                entityManager.persist(managed);
            } else {
                // Le numéro est imposé, pas de solution directe en JPA, on passe par SQL
                // il faudrait vérifier que la séquence est correcte (i.e. > à la ref )
                entityManager
                        .createNativeQuery("INSERT INTO marchandise(ref,nom,vol_unit) VALUES (:ref,'',1.0)")
                        .setParameter("ref",marchandise.getReference())
                        .executeUpdate();
                managed = entityManager.merge(managed);
            }
        } else {
            MarchandiseModele marchandise2 = MarchandiseModele.asJPA(marchandise);
            if (managed != marchandise2) {
                managed = entityManager.merge(marchandise2);
            }
        }
        return managed;
    }

    @Override
    public void remove(Marchandise marchandise) throws OperationFailedException {
        MarchandiseModele managed = findByRef(marchandise.getReference());
        if (managed != null) {
            entityManager.remove(managed);
        }
    }

}
