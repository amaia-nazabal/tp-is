package fr.univlyon1.tiw1.tp5.service;

import fr.univ_lyon1.master_info.tiw.tiw1._2017.entrepots.ApprovisionnementT;
import fr.univ_lyon1.master_info.tiw.tiw1._2017.entrepots.EntrepotT;
import fr.univ_lyon1.master_info.tiw.tiw1._2017.entrepots.LivraisonT;
import fr.univ_lyon1.master_info.tiw.tiw1._2017.entrepots.services.entrepot.*;
import fr.univlyon1.tiw1.metier.spec.*;
import fr.univlyon1.tiw1.tp5.mapping.ApprovisionnementTExtended;
import fr.univlyon1.tiw1.tp5.mapping.EntrepotTExtended;
import fr.univlyon1.tiw1.tp5.mapping.LivraisonTExtended;
import fr.univlyon1.tiw1.tp5.modele.EntrepotModele;
import fr.univlyon1.tiw1.tp5.service.exception.DataNotFoundException;
import fr.univlyon1.tiw1.tp5.service.exception.OperationFailException;
import fr.univlyon1.tiw1.tp5.util.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/13/17.
 */

@WebService(targetNamespace = Constant.NAMESPACE)
public class EntrepotPortTypeImpl implements EntrepotPortType {
    private static final Logger LOG = LoggerFactory.getLogger(EntrepotPortTypeImpl.class);

    static DatatypeFactory dtf;

    @Autowired
    private EntrepotService entrepotService;

    @Autowired
    private MarchandiseService marchandiseService;

    @Autowired
    private LivraisonService livraisonService;

    @Autowired
    private ApprovisionnementService approvisionnementService;

    static {
        try {
            dtf = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            throw new Error(e);
        }
    }

    @Override
    public boolean livrerOperation(Livrer bodyPart) {
        Livraison livraison = livraisonService.getById(Integer.parseInt(bodyPart.getId()));
        try {
            entrepotService.livrer(livraison, bodyPart.getDateLivraison().toGregorianCalendar().getTime());
            return true;
        } catch (OperationFailedException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    @Override
    public EntrepotT getByNomOperation(String bodyPart) throws NoSuchEntrepot {
        Entrepot entrepot = entrepotService.getByNom(bodyPart);

        if (entrepot == null)
            throw new NoSuchEntrepot("L'entrepot n'existe pas");

        return EntrepotTExtended.toEntrepotT(entrepot);
    }

    @Override
    public boolean receptionnerOperation(Receptionner bodyPart) {
        Approvisionnement approvisionnement = approvisionnementService.getById(Integer.parseInt(bodyPart.getId()));
        try {
            entrepotService.receptionner(approvisionnement, bodyPart.getDateReception().toGregorianCalendar().getTime());
            return true;
        } catch (OperationFailedException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }

    @Override
    @Transactional
    public ApprovisionnementT creeApprovisionnementOperation(CreeApprovisionnement bodyPart) throws OperationFailed {
        ApprovisionnementT approvisionnement = null;
        try {
            Entrepot entrepot = entrepotService.getByName(bodyPart.getEntrepot());
            Marchandise marchandise = marchandiseService.getByReference(Integer.parseInt(bodyPart.getMarchandiseRef()));

            Approvisionnement approvisionnement1 = entrepotService.creeApprovisionnement(entrepot, marchandise, bodyPart.getFournisseur(), bodyPart.getQuantite(),
                    bodyPart.getDatePrevue().toGregorianCalendar().getTime());

            approvisionnement = ApprovisionnementTExtended.toApprovisionnementT(approvisionnement1);
        } catch (DatatypeConfigurationException| DataNotFoundException|OperationFailException e) {
            LOG.error(e.getMessage());
            throw new OperationFailed(e.getMessage());
        }

        return approvisionnement;
    }

    @Override
    public LivraisonT creeLivraisonOperation(CreeLivraison bodyPart) throws OperationFailed {
        LivraisonTExtended livraisonTExtended;

        try {
            Entrepot entrepot = entrepotService.getByName(bodyPart.getEntrepot());
            Marchandise marchandise = marchandiseService.getByReference(Integer.parseInt(bodyPart.getMarchandiseRef()));

            Livraison livraison = entrepotService.creeLivraison(entrepot, marchandise, bodyPart.getMagasin(), bodyPart.getQuantite(),
                    bodyPart.getDatePrevue().toGregorianCalendar().getTime());

            livraisonTExtended = LivraisonTExtended.toLivraisonT(livraison);

        } catch (DataNotFoundException | OperationFailedException| DatatypeConfigurationException e) {
            LOG.error(e.getMessage());
            throw new OperationFailed(e.getMessage());
        }

        return livraisonTExtended;
    }

    @Override
    public boolean creerMAJOperation(CreateUpdate bodyPart) throws OperationFailed {
        Entrepot entrepot = new EntrepotModele(bodyPart.getEntrepot().getNom(), bodyPart.getEntrepot().getCapacite());
        try {
            if (!bodyPart.getEntrepot().getMarchandise().isEmpty())
                bodyPart.getEntrepot().getMarchandise()
                        .forEach(m -> {
                            Marchandise marchandise = marchandiseService.getByRef(Integer.parseInt((String)m.getRef()));
                            entrepot.ajoute(marchandise, m.getValue());
                        });

            entrepotService.createOrUpdate(entrepot);
        } catch (OperationFailedException e) {
            LOG.error(e.getMessage());
            throw new OperationFailed(e.getMessage());
        }

        return true;
    }
}
