package fr.univlyon1.tiw1.tp5.dao;

import fr.univlyon1.tiw1.metier.spec.Livraison;
import fr.univlyon1.tiw1.metier.spec.OperationFailedException;
import fr.univlyon1.tiw1.metier.spec.dao.LivraisonDAO;
import fr.univlyon1.tiw1.tp5.modele.EntrepotModele;
import fr.univlyon1.tiw1.tp5.modele.LivraisonModele;
import fr.univlyon1.tiw1.tp5.modele.MarchandiseModele;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 11/15/17.
 */
@Repository
public class LivraisonRepository implements LivraisonDAO {

    private EntityManager entityManager;
    private EntrepotRepository edao;
    private MarchandiseRepository mdao;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Autowired
    public void setEdao(EntrepotRepository edao) {
        this.edao = edao;
    }

    @Autowired
    public void setMdao(MarchandiseRepository mdao) {
        this.mdao = mdao;
    }

    @Override
    public LivraisonModele findById(int id) {
        return entityManager.find(LivraisonModele.class, id);
    }

    @Override
    public Collection<Livraison> fromEntrepot(String nom) {
        return Collections.unmodifiableCollection(
                entityManager.createNamedQuery("livraisonsFromEntrepot", LivraisonModele.class)
                        .setParameter("nom", nom)
                        .getResultList());
    }

    @Override
    public Collection<Livraison> fromMarchandise(int ref) {
        return Collections.unmodifiableCollection(
                entityManager.createNamedQuery("livraisonsFromMarchandise", LivraisonModele.class)
                        .setParameter("ref", ref)
                        .getResultList());
    }

    @Override
    public Collection<Livraison> fromMagasin(String nom) {
        return Collections.unmodifiableCollection(
                entityManager.createNamedQuery("livraisonsFromMagasin", LivraisonModele.class)
                        .setParameter("magasin", nom)
                        .getResultList()
                                                 );
    }

    @Override
    public Livraison createOrUpdate(Livraison livraison) throws OperationFailedException {
        LivraisonModele jl = findById(livraison.getId());
        if (jl == null || !(livraison instanceof LivraisonModele)) {
            boolean exists = jl != null;
            jl = copyJpaLivraison(livraison);
            if (exists) {
                return entityManager.merge(jl);
            } else {
                entityManager.persist(jl);
                return jl;
            }
        } else if (jl == livraison) {
            return livraison;
        } else {
            return entityManager.merge(livraison);
        }
    }

    private LivraisonModele copyJpaLivraison(Livraison livraison) throws OperationFailedException {
        LivraisonModele jl;
        EntrepotModele e = edao.findByNom(livraison.getNomEntrepot());
        if (e == null) {
            throw new OperationFailedException("pas d'entrepot correspondant trouvé");
        }
        MarchandiseModele m = mdao.findByRef(livraison.getRefMarchandise());
        if (m == null) {
            throw new OperationFailedException("pas de marchandise trouvée");
        }
        jl = new LivraisonModele(m, e, livraison.getMagasin(), livraison.getQuantite(), livraison.getDateCreation(), livraison.getDatePrevue());
        jl.setDateEffectuee(livraison.getDateEffectuee());
        return jl;
    }

    @Override
    public void remove(Livraison livraison) throws OperationFailedException {
        LivraisonModele jl = findById(livraison.getId());
        if (jl != null) {
            entityManager.remove(jl);
        }
    }

    @Override
    public Collection<Livraison> getAllLivraisons() throws OperationFailedException {
        return Collections.unmodifiableCollection(entityManager.createNamedQuery("allLivraisons", LivraisonModele.class).getResultList());
    }
}