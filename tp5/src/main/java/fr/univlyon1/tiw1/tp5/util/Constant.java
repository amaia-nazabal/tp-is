package fr.univlyon1.tiw1.tp5.util;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/14/17.
 */
public interface Constant {

    static String NAMESPACE = "http://master-info.univ-lyon1.fr/TIW/TIW1/2017/entrepots/services/entrepot";

    static String CREATE_OR_UPDATE_WARE = "createUpdate";

    static String GET_WARE_BY_NAME = "getByNom";

    static String CREATE_OR_UPDATE_PROVISION = "creeApprovisionnement";
}
