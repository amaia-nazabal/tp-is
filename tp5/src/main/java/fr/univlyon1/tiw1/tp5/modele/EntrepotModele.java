package fr.univlyon1.tiw1.tp5.modele;

import fr.univlyon1.tiw1.metier.spec.Entrepot;
import fr.univlyon1.tiw1.metier.spec.Marchandise;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/14/17.
 */
@Entity
@Table(name = "entrepot")
@NamedQueries({@NamedQuery(name = "allEntrepots", query = "SELECT e FROM EntrepotModele e")})
public class EntrepotModele implements Entrepot {
    @Id
    private String nom;
    private double capacite;

    // fixme verifier si le fetch est correct
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "stock", joinColumns = {@JoinColumn(name = "nom_e")})
    @Column(name = "quantite")
    @MapKeyJoinColumn(name = "ref_m")
    private Map<MarchandiseModele, Integer> quantites = new HashMap<>();

    /**
     * Empty constructor for JPA
     */
    public EntrepotModele() {
        // for JPA
    }

    /**
     * Cree un nouvel entrepot
     *
     * @param nom      le nom
     * @param capacite la capacite maximale
     */
    public EntrepotModele(String nom, double capacite) {
        this.nom = nom;
        this.capacite = capacite;
    }

    /**
     * Constructeur de clone
     *
     * @param entrepot l'entrepot à cloner
     */
    public EntrepotModele(Entrepot entrepot) {
        this(entrepot.getNom(), entrepot.getCapacite());
        for (Marchandise m : entrepot.getMarchandisesStockees()) {
            ajoute(m, entrepot.getStock(m));
        }
    }

    @Override
    public String getNom() {
        return nom;
    }

    @Override
    public double getCapacite() {
        return capacite;
    }

    @Override
    public double getOccupation() {
        return quantites.entrySet().stream()
                .mapToDouble(e -> e.getKey().getVolumeUnitaire() * e.getValue())
                .sum();
    }

    @Override
    public Collection<Marchandise> getMarchandisesStockees() {
        return Collections.unmodifiableCollection(quantites.keySet());
    }

    public void setCapacite(double capacite) {
        this.capacite = capacite;
    }

    // todo check if its okay
    @Override
    public int getStock(Marchandise marchandise) {
        MarchandiseModele asJPA = MarchandiseModele.asJPA(marchandise);
        return quantites.getOrDefault(asJPA, 0);
    }

    @Override
    public void ajoute(Marchandise m, int quantite) {
        MarchandiseModele asJPA = MarchandiseModele.asJPA(m);
        quantites.put(asJPA, quantite + getStock(asJPA));
    }

    @Override
    public void supprime(Marchandise m, int quantite) {
        MarchandiseModele asJPA = MarchandiseModele.asJPA(m);
        int actuel = getStock(asJPA);
        if (actuel <= quantite) {
            quantites.remove(asJPA);
        } else {
            quantites.put(asJPA, actuel - quantite);
        }
    }

    /**
     * Convertit un entrepot en entrepot JPA.
     * Renvoie l'entrepot passé en argument s'il s'agit déjà d'un entrepot JPA.
     *
     * @param entrepot l'entrepot à convertir
     * @return le résultat de la conversion ou l'entrepot de départ si la conversion est inutile.
     */
    public static EntrepotModele asJPA(Entrepot entrepot) {
        return entrepot instanceof EntrepotModele ?
               (EntrepotModele) entrepot : new EntrepotModele(entrepot);
    }

    /**
     * Change le stock d'un entrepot
     * @param newStock le nouvel état du stock
     */
    public void updateStock(Map<MarchandiseModele, Integer> newStock) {
        Collection<MarchandiseModele> toRemove =
                quantites.keySet().stream()
                        .filter(m -> !newStock.containsKey(m))
                        .collect(Collectors.toList());
        toRemove.forEach(quantites::remove);
        for (Map.Entry<MarchandiseModele, Integer> entry : newStock.entrySet()) {
            if (getStock(entry.getKey()) != entry.getValue()) {
                quantites.put(entry.getKey(), entry.getValue());
            }
        }
    }

    @Override
    public String toString() {
        return "Entrepot{" +
                "nom='" + nom + '\'' +
                ", capacite=" + capacite +
                ", quantites=" + quantites +
                '}';
    }
}