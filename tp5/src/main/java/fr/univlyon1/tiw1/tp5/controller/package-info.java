@XmlSchema(
        xmlns = {
                @XmlNs(prefix = "ent",
                namespaceURI="http://master-info.univ-lyon1.fr/TIW/TIW1/2017/entrepots"),
                @XmlNs(prefix = "tns",
                namespaceURI = "http://master-info.univ-lyon1.fr/TIW/TIW1/2017/entrepots/services/entrepot")
        },
        elementFormDefault = XmlNsForm.QUALIFIED,
        namespace = Constant.NAMESPACE)

package fr.univlyon1.tiw1.tp5.controller;

import fr.univlyon1.tiw1.tp5.util.Constant;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;