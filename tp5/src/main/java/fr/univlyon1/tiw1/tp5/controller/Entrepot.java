package fr.univlyon1.tiw1.tp5.controller;

import fr.univlyon1.tiw1.tp5.util.Constant;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/15/17.
 */
@XmlRootElement(name="entrepot", namespace= Constant.NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class Entrepot {

    @XmlElement(required = true)
    public String nom;

    @XmlElement(name = "capacite")
    private int capacite;

    public Entrepot() {

    }

    public Entrepot(String nom, int capacite) {
        this.nom = nom;
        this.capacite = capacite;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getCapacite() {
        return capacite;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    @Override
    public String toString() {
        return "Entrepot{" +
                "nom='" + nom + '\'' +
                ", capacite=" + capacite +
                '}';
    }
}
