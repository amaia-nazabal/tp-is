# TPs pour l'UE TIW1-IS Intergiciels et Services du master 2 Technologies de l'Information et Web - année 2017-2018

Ce dépôt constitue la base pour les TPs de l'UE.

## Présentation 

Les TPs s'appuient essentiellement sur une application visant à une gestion simplifiée d'un ensemble d'entrepôts pour une chaîne de magasins.
Le métier codé dans cette application est décrit dans le fichier [metier.md](metier.md)

Le présent projet constitue la base qui servira de point de départ pour tous les TPs de cette UEs en 2017-2018.

Le module `metier-base` comprend un ensemble de d'interfaces et de classes Java qui spécifient et implémentent une partie de l'application utilisée pour illustrer les différents concept et technlogies vus dans l'UE.

Liste des TPs:

* [TP1: révision Java](tp1/tp1.md)
* [TP2: Conteneurs](http://liris.cnrs.fr/lionel.medini/enseignement/IS/TP_conteneur.html)
* [TP3: Spring](tp3/tp3.md)
* [TP4: Clients de services](tp4/tp4.md)
* [TP5: Implémentation de services](tp5/tp5.md)
* Autres TPs à venir

Il est conseillé de lire les sujets de TP avant la première séance, même si certaines parties resteront obscures avant le premier cours.
Si vous maîtrisez déjà certaines notions (de par votre expérience personnelle), il vous est possible de commencer le TP en avance.

## Modalités de rendu

Les TPs sont à réaliser en binôme (ou éventuellement seul.e).
Les TPs qui seront évalués sont à rendre via [la forge de l'université](https://forge.univ-lyon1.fr).


Tous les rendus devront suivre les consignes suivantes:

* Un rendu est consitué d'un projet GitLab sur la forge. Dans le cadre de cette UE, le projet est identifié sur GitLab par _**son URL de clone HTTPS**_.
* Pour rendre un projet, il faut indiquer l'URL de clone HTTPS dans case appropriée sur [tomuss](https://tomuss.univ-lyon1.fr).
* Les _**deux**_ membres du binôme doivent indiquer l'URL de clone HTTPS dans tomuss: lors de la correction, les binômes sont reconsitués à partir de cette information.
* Afin qu'un projet puisse être évalué, il faut ajouter vos enseignants (`emmanuel.coquery` _**et**_ `lionel.medini`) comme membres du projet en leur donnant le rôle de _Reporter_.
* Les TPs seront clonés peu après la date de rendu. 
  La révision utilisée sera la dernière révision de la branche master avant la date de rendu.
* Il est possible d'utiliser un même projet pour tous vos TPs dans cette UE. Dans ce cas, il est vivement conseillé de _tagger_ la révision du rendu (e.g. un tag `TP2` pour la révision correspondant au rendu du TP2).

> Le non respect de ces consignes de rendu entraînera systématiquement une pénalité dans la note du TP
 
