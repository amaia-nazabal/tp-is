package fr.univlyon1.tiw1.tp3.util.exception;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/6/17.
 */
public class UtilException extends RuntimeException {
    public UtilException() {
        super();
    }

    public UtilException(String message) {
        super(message);
    }


    public UtilException(String message, Throwable cause) {
        super(message, cause);
    }


    public UtilException(Throwable cause) {
        super(cause);
    }

    protected UtilException(String message, Throwable cause, boolean enableSuppression,
                                    boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
