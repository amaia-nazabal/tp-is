package fr.univlyon1.tiw1.tp3.aspect;

import fr.univlyon1.tiw1.metier.spec.Entrepot;
import fr.univlyon1.tiw1.metier.spec.Marchandise;
import fr.univlyon1.tiw1.tp3.service.exception.DataNotFoundException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @author Amaia Nazábal
 * @version 1.0
 * @since 1.0 12/5/17.
 *
 * This class take advantage of the benefits of AOP for made some validations.
 * Like verify if the warehouse and ware exists before call the methods for create
 * new provisions or deliveries.
 *
 */

@Aspect
@Component
public class ValidationHandler {

    /**
     * This method verify than the both entities (warehouse and ware) aren't null values.
     * In other words, check if both exists in the database, before create a new provision.
     *
     * @param joinPoint the joint point
     * @param entrepot the warehouse entity
     * @param marchandise the ware entity
     */
    @Before("execution(* fr.univlyon1.tiw1.tp3.service.EntrepotService.creeApprovisionnement(..)) && args(entrepot, marchandise,..)")
    public void checkProvisionExistence(JoinPoint joinPoint, Entrepot entrepot, Marchandise marchandise) {
        if (entrepot == null) {
            throw new DataNotFoundException("L'entrepot n'existe pas.");
        }

        if (marchandise == null) {
            throw new DataNotFoundException("La marchandise n'existe pas.");
        }
    }

    /**
     * This method verify than the both entities (warehouse and ware) aren't null values.
     * In other words, check if both exists in the database, before create a new delivery.
     *
     * @param joinPoint the joint point
     * @param entrepot the warehouse entity
     * @param marchandise the ware entity
     */
    @Before("execution(* fr.univlyon1.tiw1.tp3.service.EntrepotService.creeLivraison(..)) && args(entrepot, marchandise,..)")
    public void checkDeliveryExistence(JoinPoint joinPoint, Entrepot entrepot, Marchandise marchandise) {
        if (entrepot == null) {
            throw new DataNotFoundException("L'entrepot n'existe pas.");
        }

        if (marchandise == null) {
            throw new DataNotFoundException("La marchandise n'existe pas.");
        }
    }

}
